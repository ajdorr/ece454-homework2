/********************************************************
 * Kernels to be optimized for the CS:APP Performance Lab
 ********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

/* 
 * ECE454 Students: 
 * Please fill in the following team struct 
 */
team_t team = {
	"Insurrection",              /* Team name */

	"Jarrod Stone",     /* First member full name */
	"stonejar@ecf.utoronto.ca",  /* First member email address */

	"",                   /* Second member full name (leave blank if none) */
	""                    /* Second member email addr (leave blank if none) */
};
/*************** * ROTATE KERNEL
 ***************/

/******************************************************
 * Your different versions of the rotate kernel go here
 ******************************************************/

/* 
 * naive_rotate - The naive baseline version of rotate 
 */
char naive_rotate_descr[] = "naive_rotate: Naive baseline implementation";
void naive_rotate(int dim, pixel *src, pixel *dst) 
{
	int i, j;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
			// printf("%d, %d", dim -1-j, i);
		}
	}
}

/*
 * ECE 454 Students: Write your rotate functions here:
 */ 

/* 
 * rotate - Your current working version of rotate
 * IMPORTANT: This is the version you will be graded on
 */
char rotate_descr[] = "rotate: Current working version";
void rotate(int dim, pixel *src, pixel *dst) 
{
	register int i, j, limit = dim*dim, sub = limit+1;

	j=dim-1;
	i=0;
	// for (i = 0; i < limit; i++) {
	while (j >= 0) {
		while (j<limit) {
			dst[i] = src[j];

			i++;
			j+=dim;
		}
		j-=sub;
	}

}


/* 
 * second attempt (commented out for now)
 */
char rotate_two_descr[] = "second attempt";
void attempt_two(int dim, pixel *src, pixel *dst) 
{
	register unsigned int i, limit = dim*dim;

	for (i=0; i< limit; i++) {
		dst[RIDX(dim - i%dim - 1, i/dim, dim)] = src[i];
	}
}

char rotate_three_descr[] = "trying by jumping and stuff...";
void attempt_three(int dim, pixel *src, pixel *dst) 
{
	register int i, j, limit = dim*dim, sub = limit+1;

	j=dim-1;
	for (i = dim*dim-1; i >=0; i--) {
		// printf("i: %d, j: %d, dim: %d\n", i,j,dim);
		dst[j] = src[i];
		j+=dim;
		if (j>limit) j-=sub;
	}

}

char rotate_four_descr[] = "trying by jumping and stuff...";
void attempt_four(int dim, pixel *src, pixel *dst) 
{
	register int i, j, limit = dim*dim, sub = limit+1;

	j=dim-1;
	i=0;
	// for (i = 0; i < limit; i++) {
	while (j >= 0) {
		while (j<limit) {
			dst[i] = src[j];

			i++;
			j+=dim;
		}
		j-=sub;
	}

}


char rotate_five_descr[] = "trying a new algorithm";
void attempt_five(int dim, pixel *src, pixel *dst)  
{
	int num = 4;
	int count = dim >> num;
	int stride = 1 << num;
	src += dim - 1; 
	int a1 = count;
	do {
		int a2 = dim;
		do {
			int a3 = stride;
			do {
				*dst++ = *src;
				src += dim;
			} while(--a3);
			src -= dim * stride + 1;
			dst += dim - stride;
		} while(--a2);
		src += dim * (stride + 1);
		dst -= dim * dim - stride;
	} while(--a1);
}

char rotate_six_descr[] = "trying a new algorithm";
void attempt_six(int dim, pixel *src, pixel *dst)  
{
	int i, j, ii, jj;
	for(jj = 0; jj < dim; jj += 8)
		for(ii = 0; ii < dim; ii += 8)
			for (i = ii; i < ii + 8; i++)
				for (j = jj; j < jj + 8; j++)
					dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
}




/*********************************************************************
 * register_rotate_functions - Register all of your different versions
 *     of the rotate kernel with the driver by calling the
 *     add_rotate_function() for each test function. When you run the
 *     driver program, it will test and report the performance of each
 *     registered test function.  
 *********************************************************************/

void register_rotate_functions() 
{
	add_rotate_function(&naive_rotate, naive_rotate_descr);   
	// add_rotate_function(&rotate, rotate_descr);   
	// add_rotate_function(&attempt_two, rotate_two_descr);   
	//add_rotate_function(&attempt_three, rotate_three_descr);   
	//add_rotate_function(&attempt_four, rotate_four_descr);   
	add_rotate_function(&attempt_five, rotate_five_descr);   
	//add_rotate_function(&attempt_six, rotate_six_descr);   
	//add_rotate_function(&attempt_seven, rotate_seven_descr);   
	//add_rotate_function(&attempt_eight, rotate_eight_descr);   
	//add_rotate_function(&attempt_nine, rotate_nine_descr);   
	//add_rotate_function(&attempt_ten, rotate_ten_descr);   
	//add_rotate_function(&attempt_eleven, rotate_eleven_descr);   

	/* ... Register additional rotate functions here */
}

